const { response } = require("express");
const { query } = require("../database/conectar_db");
const querys = require("../helpers/querys");
const bcrypt = require("bcrypt");

const changePassword = async(req, res = response) => {
    if ([3, 4].includes(req.id_rol))
        return res
            .status(401)
            .json({ ok: false, body: "Don't have authentication" });
    let { username } = req.body;
    if (!username) {
        return res.status(400).json({
            ok: false,
            message: "Username is required",
        });
    }
    try {
        let user = await querys.user(username);

        if (user != 0) {
            let password = bcrypt.hashSync(
                req.body["password"],
                Number(process.env.AUTH_ROUNDS)
            );
            console.log(username);
            console.log(password);
            await querys.change_KeyPassword(username, password);

            return res.status(400).json({
                ok: true,
                message: "Contraseña cambiada",
            });
        } else {
            return res.status(401).json({
                ok: false,
                message: "Usuario no encontrado",
            });
        }
    } catch (error) {
        return res.status(403).json({
            ok: false,
            error,
        });
    }
};

module.exports = {
    changePassword,
};