require("dotenv").config();
require("./middlewares/emqx");

const Server = require("./models/server");

const server = new Server();

server.listen();