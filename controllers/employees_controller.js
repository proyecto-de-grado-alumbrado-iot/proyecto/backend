const { response } = require("express");
const EmployeeModel = require("../models/employee_model");
const CrewModel = require("../models/crew_model");
const CommuneModel = require("../models/commune_model");
const querys = require("../helpers/querys");

const employee = {};
const regex = /^[0-9]+$/;

//Se guardan en constantes los callbacks

/*
 
  ######   ######## ########    ######## ##     ## ########  ##        #######  ##    ## ######## ########  ######     ########  ##    ##    #### ########  
 ##    ##  ##          ##       ##       ###   ### ##     ## ##       ##     ##  ##  ##  ##       ##       ##    ##    ##     ##  ##  ##      ##  ##     ## 
 ##        ##          ##       ##       #### #### ##     ## ##       ##     ##   ####   ##       ##       ##          ##     ##   ####       ##  ##     ## 
 ##   #### ######      ##       ######   ## ### ## ########  ##       ##     ##    ##    ######   ######    ######     ########     ##        ##  ##     ## 
 ##    ##  ##          ##       ##       ##     ## ##        ##       ##     ##    ##    ##       ##             ##    ##     ##    ##        ##  ##     ## 
 ##    ##  ##          ##       ##       ##     ## ##        ##       ##     ##    ##    ##       ##       ##    ##    ##     ##    ##        ##  ##     ## 
  ######   ########    ##       ######## ##     ## ##        ########  #######     ##    ######## ########  ######     ########     ##       #### ########  
 
*/
/**
 * 
 @todo Arreglar la respuesta
 */
employee.getEmployeeById = async(req, res = response) => {
    try {
        const id = req.params.id;

        if (!id ) {
            return res.status(400).json({
                message: "El id es obligatorio",
                body: null,
            });
        }
        const infoCrewEmploy = await querys.getIdCrew(id);

        return res.status(200).json({
            ok:true,
            body: infoCrewEmploy? infoCrewEmploy[0]["id-crew"]:null,
        });
    } catch (e) {
        return res.status(500).json({
            message: `${e}`,
            body: null,
        });
    }
};

/*
 
 ##     ## ########  ########     ###    ######## ########    ######## ##     ## ########  ##        #######  ##    ## ######## ######## 
 ##     ## ##     ## ##     ##   ## ##      ##    ##          ##       ###   ### ##     ## ##       ##     ##  ##  ##  ##       ##       
 ##     ## ##     ## ##     ##  ##   ##     ##    ##          ##       #### #### ##     ## ##       ##     ##   ####   ##       ##       
 ##     ## ########  ##     ## ##     ##    ##    ######      ######   ## ### ## ########  ##       ##     ##    ##    ######   ######   
 ##     ## ##        ##     ## #########    ##    ##          ##       ##     ## ##        ##       ##     ##    ##    ##       ##       
 ##     ## ##        ##     ## ##     ##    ##    ##          ##       ##     ## ##        ##       ##     ##    ##    ##       ##       
  #######  ##        ########  ##     ##    ##    ########    ######## ##     ## ##        ########  #######     ##    ######## ######## 
 
*/
employee.updateEmploye = async(req, res = response) => {
    try {
        const id = req.params.id;
        const body = req.body;
        let argumentsUpd = {};

        if (!id || !regex.test(id)) {
            return res.status(400).json({
                message: "El id debe ser un entero",
                body: null,
            });
        }

        if (body["phone"]) argumentsUpd["phone-employee"] = body["phone"];
        if (body["crew"]) argumentsUpd["id-crew"] = body["crew"];
        if (body["title"]) argumentsUpd["title-employee"] = body["title"];

        await EmployeeModel.update(argumentsUpd, {
                where: {
                    "id-employee": id,
                },
            })
            .then((resp) => {
                return res.status(200).json({
                    message: `se actualizaron ${resp} registros`,
                    body: null,
                });
            })
            .catch((e) => {
                throw new Error(`${e}`);
            });
    } catch (e) {
        return res.status(500).json({
            message: `${e}`,
            body: null,
        });
    }
};

/*
 
  ######  ########  ########    ###    ######## ########    ######## ##     ## ########  ##        #######  ##    ## ######## ######## 
 ##    ## ##     ## ##         ## ##      ##    ##          ##       ###   ### ##     ## ##       ##     ##  ##  ##  ##       ##       
 ##       ##     ## ##        ##   ##     ##    ##          ##       #### #### ##     ## ##       ##     ##   ####   ##       ##       
 ##       ########  ######   ##     ##    ##    ######      ######   ## ### ## ########  ##       ##     ##    ##    ######   ######   
 ##       ##   ##   ##       #########    ##    ##          ##       ##     ## ##        ##       ##     ##    ##    ##       ##       
 ##    ## ##    ##  ##       ##     ##    ##    ##          ##       ##     ## ##        ##       ##     ##    ##    ##       ##       
  ######  ##     ## ######## ##     ##    ##    ########    ######## ##     ## ##        ########  #######     ##    ######## ######## 
 
*/
employee.createEmployee = async(req, res = response) => {
    try {
        const body = req.body;
        let employeeBuild;
        await EmployeeModel.create(body, {
                include: [{
                    model: CrewModel,
                }, ],
            })
            .then((resp) => {
                employeeBuild = resp;
            })
            .catch((e) => {
                throw new Error(`${e}`);
            });
        return res.status(200).json({
            message: "ok",
            body: employeeBuild,
        });
    } catch (e) {
        return res.status(500).json({
            message: `${e}`,
            body: null,
        });
    }
};

/*
 
  ######  ########  ########    ###    ######## ########     ######  ########  ######## ##      ## 
 ##    ## ##     ## ##         ## ##      ##    ##          ##    ## ##     ## ##       ##  ##  ## 
 ##       ##     ## ##        ##   ##     ##    ##          ##       ##     ## ##       ##  ##  ## 
 ##       ########  ######   ##     ##    ##    ######      ##       ########  ######   ##  ##  ## 
 ##       ##   ##   ##       #########    ##    ##          ##       ##   ##   ##       ##  ##  ## 
 ##    ## ##    ##  ##       ##     ##    ##    ##          ##    ## ##    ##  ##       ##  ##  ## 
  ######  ##     ## ######## ##     ##    ##    ########     ######  ##     ## ########  ###  ###  
 
*/

employee.createCrew = async(req, res = response) => {
    try {
        const body = req.body;
        let crewBuild;
        await CrewModel.create(body, {
                include: [{
                    model: CommuneModel,
                }, ],
            })
            .then((resp) => {
                crewBuild = resp;
            })
            .catch((e) => {
                throw new Error(`${e}`);
            });
        return res.status(200).json({
            message: "ok",
            body: crewBuild,
        });
    } catch (e) {
        return res.status(500).json({
            message: `${e}`,
            body: null,
        });
    }
};

/*
 
  ######  ##     ##    ###    ##    ##  ######   ########    ######## ##     ## ########  ##        #######  ##    ## ######## ########     ######  ########  ######## ##      ## 
 ##    ## ##     ##   ## ##   ###   ## ##    ##  ##          ##       ###   ### ##     ## ##       ##     ##  ##  ##  ##       ##          ##    ## ##     ## ##       ##  ##  ## 
 ##       ##     ##  ##   ##  ####  ## ##        ##          ##       #### #### ##     ## ##       ##     ##   ####   ##       ##          ##       ##     ## ##       ##  ##  ## 
 ##       ######### ##     ## ## ## ## ##   #### ######      ######   ## ### ## ########  ##       ##     ##    ##    ######   ######      ##       ########  ######   ##  ##  ## 
 ##       ##     ## ######### ##  #### ##    ##  ##          ##       ##     ## ##        ##       ##     ##    ##    ##       ##          ##       ##   ##   ##       ##  ##  ## 
 ##    ## ##     ## ##     ## ##   ### ##    ##  ##          ##       ##     ## ##        ##       ##     ##    ##    ##       ##          ##    ## ##    ##  ##       ##  ##  ## 
  ######  ##     ## ##     ## ##    ##  ######   ########    ######## ##     ## ##        ########  #######     ##    ######## ########     ######  ##     ## ########  ###  ###  
 
*/

employee.changeEmployeeCrew = async(req, res = response) => {
    try {
        const employee = req.params.id;
        const crew = req.query.crew;
        console.log(employee, crew);
        if (!crew || !regex.test(crew) || !employee || !regex.test(employee)) {
            return res.status(400).json({
                message: "Error en los parametros",
                body: null,
            });
        }

        await EmployeeModel.update({ "id-crew": crew }, {
                where: {
                    "id-employee": employee,
                },
            })
            .then((resp) => {
                return res.status(200).json({
                    message: `se actualizaron ${resp} registros`,
                    body: null,
                });
            })
            .catch((e) => {
                throw new Error(`${e}`);
            });
    } catch (e) {
        return res.status(500).json({
            message: `${e}`,
            body: null,
        });
    }
};

/*
 
  ######   ######## ########    #### ##    ## ########  #######     ##     ##  ######  ######## ########  
 ##    ##  ##          ##        ##  ###   ## ##       ##     ##    ##     ## ##    ## ##       ##     ## 
 ##        ##          ##        ##  ####  ## ##       ##     ##    ##     ## ##       ##       ##     ## 
 ##   #### ######      ##        ##  ## ## ## ######   ##     ##    ##     ##  ######  ######   ########  
 ##    ##  ##          ##        ##  ##  #### ##       ##     ##    ##     ##       ## ##       ##   ##   
 ##    ##  ##          ##        ##  ##   ### ##       ##     ##    ##     ## ##    ## ##       ##    ##  
  ######   ########    ##       #### ##    ## ##        #######      #######   ######  ######## ##     ## 
 
*/
employee.getInfoUser = async(req, res) => {
    if ([3,4].includes(req.id_rol))
        return res
            .status(401)
            .json({ ok: false, body: "Don't have authentication" });
    try {
     
        let getInfo = await querys.usersInfo();
        return res.json({
            ok: true,
            body: getInfo[0],
        });
    } catch (error) {
       return res.status(500).json({
            ok: false,
            error,
        });
    }
};
module.exports = employee;