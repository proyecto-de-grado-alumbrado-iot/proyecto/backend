const express = require("express");
const router = express.Router();
const device = require("../controllers/devices_controllers");
const { validatorJWT } = require("../middlewares/jwt-validator");

//------------post---------------------

router.get("/:idFailure", [validatorJWT], device.updateFault);

module.exports = router;