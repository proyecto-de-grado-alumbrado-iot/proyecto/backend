const { Router } = require("express");
const { verifyToken } = require("../controllers/auth_controller");
const auth = require("../controllers/auth_controller");
const { validatorJWT } = require("../middlewares/jwt-validator");
const { changePassword } = require("../middlewares/chancePass");

const router = Router();

//------------get---------------------
router.get("/verifyToken", [validatorJWT], auth.verifyToken);

//------------post---------------------
router.post("/signIn", auth.signIn);
router.post("/signUp", auth.signUp);
router.post("/change", [validatorJWT], changePassword);
//------------put---------------------

//------------delete---------------------

module.exports = router;