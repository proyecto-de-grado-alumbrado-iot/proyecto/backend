const Sequelize = require("sequelize");

const conn = require("../database/conectar_db");

const DepartmentModel = require("../models/department_model");

const CityModel = conn.define(
    "city", {
        "id-city": {
            type: Sequelize.INTEGER,
            autoIncrement: true,
            allowNull: false,
            primaryKey: true,
        },
        "name-city": {
            type: Sequelize.STRING,
            allowNull: false,
        },

        // 'id-department': {
        //   type: Sequelize.INTEGER,
        //   allowNull: false,

        // },
    }, {
        //------- NO AGREGA EL CREATEAT Y UPDATEAT --------------------//
        timestamps: false,
        //------- EVITA QUE CAMBIE EL NOMBRE DE LA TABLA --------------//
        freezeTableName: true,
        //------- NOMBRE DE LA TABLA EN LA BASE DE DATOS --------------//
        tableName: "city",
    }
);

// Relación 1:N entre Department y City
CityModel.belongsTo(DepartmentModel, {
    foreignKey: "id-department",
    constraints: false,
});
DepartmentModel.hasMany(CityModel, {
    foreignKey: "id-department",
    constraints: false,
});

module.exports = CityModel;