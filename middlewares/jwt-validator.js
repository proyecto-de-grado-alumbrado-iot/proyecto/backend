const { response } = require("express");
const jwt = require("jsonwebtoken");

const validatorJWT = (req, res = response, next) => {
    //handle errors
    const token = req.header("x-token");
    if (!token) {
        return res.status(401).json({
            ok: false,
            errors: "Token doesn't provider",
        });
    }
    try {
        const payload = jwt.verify(token, process.env.AUTH_SECRET);
        // console.log(payload);
        const { user } = payload;
        req.id_rol = user["id-rol"];
        req.name_user = user["name-user"];
        req.email_user = user["email-user"];
        next();
    } catch (error) {
        return res.status(403).json({
            ok: false,
            error,
        });
    }
};

module.exports = {
    validatorJWT,
};