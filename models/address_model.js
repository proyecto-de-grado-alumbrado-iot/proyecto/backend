const Sequelize = require("sequelize");

const conn = require("../database/conectar_db");

const CommuneModel = require("../models/commune_model");

const AddressModel = conn.define(
    "address", {
        "id-address": {
            type: Sequelize.INTEGER,
            autoIncrement: true,
            allowNull: false,
            primaryKey: true,
        },
        "name-address": {
            type: Sequelize.STRING,
            allowNull: false,
        },

        // 'id-commune': {
        //   type: Sequelize.INTEGER,
        //   allowNull: false,

        // },
    }, {
        //------- NO AGREGA EL CREATEAT Y UPDATEAT --------------------//
        timestamps: false,
        //------- EVITA QUE CAMBIE EL NOMBRE DE LA TABLA --------------//
        freezeTableName: true,
        //------- NOMBRE DE LA TABLA EN LA BASE DE DATOS --------------//
        tableName: "address",
    }
);

// Relación 1:N entre Commune y Address
AddressModel.belongsTo(CommuneModel, {
    foreignKey: "id-commune",
    constraints: false,
});
CommuneModel.hasMany(AddressModel, {
    foreignKey: "id-commune",
    constraints: false,
});

module.exports = AddressModel;