const Sequelize = require("sequelize");

const conn = require("../database/conectar_db");

const UserModel = require("../models/user_model");
const CrewModel = require("../models/crew_model");

const EmployeeModel = conn.define(
    "employee", {
        "id-employee": {
            type: Sequelize.INTEGER,
            autoIncrement: true,
            allowNull: false,
            primaryKey: true,
        },

        "lastname-employee": {
            type: Sequelize.STRING,
            allowNull: false,
        },

        "name-employee": {
            type: Sequelize.STRING,
            allowNull: false,
        },

        "phone-employee": {
            type: Sequelize.STRING,
            allowNull: false,
        },

        "photo-employee": Sequelize.STRING,
        // 'id-user': Sequelize.STRING,
        // 'id-crew': Sequelize.INTEGER,

        "born-date-employee": {
            type: Sequelize.DATEONLY,
            allowNull: false,
        },

        "title-employee": {
            type: Sequelize.STRING,
            allowNull: false,
        },
        "created-at": {
            type: Sequelize.DATE,
        },
        "created-by": {
            type: Sequelize.STRING,
        },
        "updated-at": {
            type: Sequelize.DATE,
        },
        "updated-by": {
            type: Sequelize.STRING,
        },
    }, {
        //------- AGREGA EL CREATEAT Y UPDATEAT --------------------//
        timestamps: false,
        //------- EVITA QUE CAMBIE EL NOMBRE DE LA TABLA --------------//
        freezeTableName: true,
        //------- NOMBRE DE LA TABLA EN LA BASE DE DATOS --------------//
        tableName: "employee",
    }
);

// Relación 1:1 entre User y Employees
EmployeeModel.belongsTo(UserModel, {
    foreignKey: "id-user",
    constraints: false,
});
UserModel.hasOne(EmployeeModel, { foreignKey: "id-user", constraints: false });

// Relación 1:N entre Crew y Employee
EmployeeModel.belongsTo(CrewModel, {
    foreignKey: "id-crew",
    constraints: false,
});
CrewModel.hasMany(EmployeeModel, { foreignKey: "id-crew", constraints: false });

module.exports = EmployeeModel;