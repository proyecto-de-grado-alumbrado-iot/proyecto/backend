const { Router } = require('express');
const device = require('../controllers/devices_controllers');
const { validatorJWT } = require('../middlewares/jwt-validator');

const router = Router();

//------------get---------------------
router.get('/getComunas', [validatorJWT], device.getComunas);
router.get('/:id', [validatorJWT],device.getDeviceById);
router.get('/', [validatorJWT], device.getAllDevices);

//------------post---------------------
router.post('/', [validatorJWT],device.createDevice);

//------------delete---------------------
router.delete('/:id', [validatorJWT],device.deleteDevice);

//------------put---------------------
router.put('/:id',[validatorJWT], device.updateDevice);


module.exports = router;