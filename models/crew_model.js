const Sequelize = require("sequelize");

const conn = require("../database/conectar_db");

const CommuneModel = require("../models/commune_model");

const CrewModel = conn.define(
    "crew", {
        "id-crew": {
            type: Sequelize.INTEGER,
            autoIncrement: true,
            allowNull: false,
            primaryKey: true,
        },

        "id-commune": {
            type: Sequelize.INTEGER,
            allowNull: false,
        },

        "name-crew": {
            type: Sequelize.STRING,
            allowNull: false,
        },
    }, {
        //------- NO AGREGA EL CREATEAT Y UPDATEAT --------------------//
        timestamps: false,
        //------- EVITA QUE CAMBIE EL NOMBRE DE LA TABLA --------------//
        freezeTableName: true,
        //------- NOMBRE DE LA TABLA EN LA BASE DE DATOS --------------//
        tableName: "crew",
    }
);

// Relación 1:N entre Commune y Crew
CrewModel.belongsTo(CommuneModel, {
    foreignKey: "id-commune",
    constraints: false,
});
CommuneModel.hasMany(CrewModel, {
    foreignKey: "id-commune",
    constraints: false,
});

module.exports = CrewModel;