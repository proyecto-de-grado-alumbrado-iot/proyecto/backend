const { response } = require("express");
const DeviceModel = require("../models/device_model");
const AddressModel = require("../models/address_model");
const CommuneModel = require("../models/commune_model");
const CityModel = require("../models/city_model");
const DepartmentModel = require("../models/department_model");
const querys = require("../helpers/querys");

const device = {};
const regex = /^[0-9]+$/;

/*

  ######   ######## ########    ########  ######## ##     ## ####  ######  ########    ########  ##    ##    #### ########  
 ##    ##  ##          ##       ##     ## ##       ##     ##  ##  ##    ## ##          ##     ##  ##  ##      ##  ##     ## 
 ##        ##          ##       ##     ## ##       ##     ##  ##  ##       ##          ##     ##   ####       ##  ##     ## 
 ##   #### ######      ##       ##     ## ######   ##     ##  ##  ##       ######      ########     ##        ##  ##     ## 
 ##    ##  ##          ##       ##     ## ##        ##   ##   ##  ##       ##          ##     ##    ##        ##  ##     ## 
 ##    ##  ##          ##       ##     ## ##         ## ##    ##  ##    ## ##          ##     ##    ##        ##  ##     ## 
  ######   ########    ##       ########  ########    ###    ####  ######  ########    ########     ##       #### ########  
 
*/
/**
     * @method getDeviceById devuelve el dispositivo que corresponde al id enviado
     * 
     * @param { int } id se recibe por ruta, es obligatorio
 
     * @returns { Object } {
       DeviceModel
     }
     * @author Maria Vera 
     */

device.getDeviceById = async(req, res = response) => {
    try {
        const id = req.params.id;

        if (!id || !regex.test(id)) {
            return res.status(400).json({
                ok: false,
                message: "El id debe ser un entero",
                body: null,
            });
        }

        let device;
        const resp = await DeviceModel.findByPk(
            id, {
                include: [{
                    model: AddressModel,
                    required: true,

                    include: [{
                        model: CommuneModel,
                        required: true,

                        include: [{
                            model: CityModel,
                            required: true,

                            include: [{
                                model: DepartmentModel,
                                required: true,

                                attributes: ["name-department"],
                            }, ],
                            attributes: ["name-city"],
                        }, ],
                        attributes: ["name-commune"],
                    }, ],
                    attributes: ["name-address"],
                }, ],
            }, { raw: true }
        );

        if (resp === null) {
            device = null;
        } else {
            device = resp.dataValues;
            device["id-address"] = undefined;

            const address = device.address.dataValues;
            device.address = address["name-address"];

            const commune = address.commune.dataValues;
            device.commune = commune["name-commune"];

            const city = commune.city.dataValues;
            device.city = city["name-city"];

            device.department = city.department.dataValues["name-department"];
        }

        return res.status(200).json({
            ok: true,

            message: "Ok",
            body: device,
        });
    } catch (e) {
        return res.status(500).json({
            ok: false,

            message: `${e}`,
            body: null,
        });
    }
};
/*
     
      ######   ######## ########       ###    ##       ##          ########  ######## ##     ## ####  ######  ########  ######  
     ##    ##  ##          ##         ## ##   ##       ##          ##     ## ##       ##     ##  ##  ##    ## ##       ##    ## 
     ##        ##          ##        ##   ##  ##       ##          ##     ## ##       ##     ##  ##  ##       ##       ##       
     ##   #### ######      ##       ##     ## ##       ##          ##     ## ######   ##     ##  ##  ##       ######    ######  
     ##    ##  ##          ##       ######### ##       ##          ##     ## ##        ##   ##   ##  ##       ##             ## 
     ##    ##  ##          ##       ##     ## ##       ##          ##     ## ##         ## ##    ##  ##    ## ##       ##    ## 
      ######   ########    ##       ##     ## ######## ########    ########  ########    ###    ####  ######  ########  ######  
     
    */
/**
         * @method getAllDevices devuelve los dispositivos paginados, si se envian los parametros de lugar los filtra, si no los devuelve todos
         * 
         * @param { int } deparment se recibe por queryParams, es opcional
         * @param { int } city se recibe por queryParams y debe haberse enviado el department,es opcional
         * @param { int } commune se recibe por queryParams y debe haberse enviado el department y city, es opcional
         * @param { int } page se recibe por queryParams, por default es 1

         * @returns { Object } {
           * "rows": DevicesModel[ ],
           * "total": int,
           * "per-page": int,
           * "last-page": int,
           * "current-page": int
         }
         * @author Maria Vera 
         */
device.getAllDevices = async(req, res = response) => {
    try {
        const body = req.query;
        let whereDev= {};
        let whereCom = {};
        let devices = [];
        let page = 1;

       
        // ---------- validamos los parametros del body------
        
        if (body.commune && regex.test(body.commune)) {
            
            whereCom = {
                "id-commune": body.commune,
            };
           
        }

        if(body.idDev && regex.test(body.idDev)){
            whereDev = {
                "id-device": body.idDev,
            };
        }

        await DeviceModel.findAndCountAll({
            where:whereDev,
                raw: true,
                include: [{
                    model: AddressModel,
                    required: true,

                    include: [{
                        where: whereCom,
                        model: CommuneModel,
                        required: true,

                        include: [{
                            model: CityModel,
                            required: true,

                            include: [{
                                model: DepartmentModel,
                                required: true,

                                attributes: ["name-department"],
                            }, ],
                            attributes: ["name-city"],
                        }, ],
                        attributes: ["name-commune"],
                    }, ],
                    attributes: ["name-address"],
                }, ],
            }, { raw: true })
            .then((resp) => {
                devices = resp;
                //------- armamos la respuesta ---------

                if (devices.rows.length === 0) return (devices.rows = null);

                devices["total"] = devices.count;
                devices.count = undefined;

                devices["per-page"] = devices.rows.length;
                devices["last-page"] = Math.ceil(devices.total / 2);
                devices["current-page"] = parseInt(page);

                resp.rows.forEach((item, i) => {
                    devices.rows[i]["id-address"] = undefined;
                    devices.rows[i].address = devices.rows[i]["address.name-address"];
                    devices.rows[i]["address.name-address"] = undefined;

                    devices.rows[i]["address.commune.id-commune"] = undefined;
                    devices.rows[i].commune =
                        devices.rows[i]["address.commune.name-commune"];
                    devices.rows[i]["address.commune.name-commune"] = undefined;

                    devices.rows[i]["address.commune.city.id-city"] = undefined;
                    devices.rows[i].city =
                        devices.rows[i]["address.commune.city.name-city"];
                    devices.rows[i]["address.commune.city.name-city"] = undefined;

                    devices.rows[i]["address.commune.city.department.id-department"] =
                        undefined;
                    devices.rows[i].department =
                        devices.rows[i]["address.commune.city.department.name-department"];
                    devices.rows[i]["address.commune.city.department.name-department"] =
                        undefined;
                });
            })
            .catch((err) => {
                throw new Error(err);
            });

        return res.status(200).json({
            ok: true,

            message: "Ok",
            body: devices,
        });
    } catch (e) {
        throw new Error(e);
    }
};

/*
 
  ######  ########  ########    ###    ######## ########    ########  ######## ##     ## ####  ######  ######## 
 ##    ## ##     ## ##         ## ##      ##    ##          ##     ## ##       ##     ##  ##  ##    ## ##       
 ##       ##     ## ##        ##   ##     ##    ##          ##     ## ##       ##     ##  ##  ##       ##       
 ##       ########  ######   ##     ##    ##    ######      ##     ## ######   ##     ##  ##  ##       ######   
 ##       ##   ##   ##       #########    ##    ##          ##     ## ##        ##   ##   ##  ##       ##       
 ##    ## ##    ##  ##       ##     ##    ##    ##          ##     ## ##         ## ##    ##  ##    ## ##       
  ######  ##     ## ######## ##     ##    ##    ########    ########  ########    ###    ####  ######  ######## 
 
*/
/**
     * @method createDevice crea un nuevo dispositivo
     * 
     * @param { Object } body
     *  
    * {
        "name-device": "LUM-345",
        "lat-device": 7.105955414287668,
        "lon-device": -73.1142080730143,
        "description-luminary": "LUMINARIA ANTIGUA",
        "normal-shunt-voltage": 0.95,
        "normal-current": 2,
        "normal-power": 0.15,
        "working": 0,
        "address": 
        {
            "name-address": "Provenza",
            "id-commune": 10
        } 
        "id-address": 1
    }
     * @returns { Object } el objeto creado
     * @author Maria Vera 
     */

device.createDevice = async(req, res = response) => {
    try {
        const body = req.body;
        body["created-by"] = req.email_user;
        body["updated-by"] = req.email_user;
        let deviceBuild;
        await DeviceModel.create(body, {
                include: [{
                    model: AddressModel,
                }, ],
            })
            .then((resp) => {
                deviceBuild = resp;

                deviceBuild.address = undefined;
            })
            .catch((e) => {
                throw new Error(`${e}`);
            });
        return res.status(200).json({
            ok: true,

            message: "ok",
            body: deviceBuild,
        });
    } catch (e) {
        return res.status(500).json({
            ok: false,

            message: `${e}`,
            body: null,
        });
    }
};

/*
 
 ########  ######## ##       ######## ######## ########    ########  ######## ##     ## ####  ######  ######## 
 ##     ## ##       ##       ##          ##    ##          ##     ## ##       ##     ##  ##  ##    ## ##       
 ##     ## ##       ##       ##          ##    ##          ##     ## ##       ##     ##  ##  ##       ##       
 ##     ## ######   ##       ######      ##    ######      ##     ## ######   ##     ##  ##  ##       ######   
 ##     ## ##       ##       ##          ##    ##          ##     ## ##        ##   ##   ##  ##       ##       
 ##     ## ##       ##       ##          ##    ##          ##     ## ##         ## ##    ##  ##    ## ##       
 ########  ######## ######## ########    ##    ########    ########  ########    ###    ####  ######  ######## 
 
*/
/**
     * @method deleteDevice elimina un dispositivo
     * 
   
     * @param { int } id va en la ruta y es el id de dispositivo que se va a editar
     * @returns { Object } la respuesta retorna un mensaje con la cantidad de archivos eliminados y el body null 
     * @author Maria Vera
     */

device.deleteDevice = async(req, res = response) => {
    try {
        const id = req.params.id;
        if (!id || !regex.test(id)) {
            return res.status(400).json({
                ok: false,

                message: "El id debe ser un entero",
                body: null,
            });
        }
        await DeviceModel.destroy({
                where: {
                    "id-device": id,
                },
            })
            .then((resp) => {
                return res.status(200).json({
                    ok: true,

                    message: `se eliminaron ${resp} registros`,
                    body: null,
                });
            })
            .catch((e) => {
                throw new Error(e);
            });
    } catch (err) {
        throw new Error(err);
    }
};

/*
 
 ##     ## ########  ########     ###    ######## ########    ########  ######## ##     ## ####  ######  ######## 
 ##     ## ##     ## ##     ##   ## ##      ##    ##          ##     ## ##       ##     ##  ##  ##    ## ##       
 ##     ## ##     ## ##     ##  ##   ##     ##    ##          ##     ## ##       ##     ##  ##  ##       ##       
 ##     ## ########  ##     ## ##     ##    ##    ######      ##     ## ######   ##     ##  ##  ##       ######   
 ##     ## ##        ##     ## #########    ##    ##          ##     ## ##        ##   ##   ##  ##       ##       
 ##     ## ##        ##     ## ##     ##    ##    ##          ##     ## ##         ## ##    ##  ##    ## ##       
  #######  ##        ########  ##     ##    ##    ########    ########  ########    ###    ####  ######  ######## 
 
*/

/**
 * @method updateDevice actualiza los atributos del dispositivo
 *
 * @param { Object } body Objeto que puede contener 1 o varios de los siguientes atributos
 *
 * {
 *    "device": "LUM-345",
 *   "lat": 7.105955414287668,
 *  "lon": -73.1142080730143,
 *  "description": "LUMINARIA ANTIGUA",
 * "voltage": 0.95,
 *  "current": 2,
 *   "power": 0.15,
 *    "working": 0,
 *     "address": 1
 *}
 * @param { int } id va en la ruta y es el id de dispositivo que se va a editar
 * @returns { Object } la respuesta retorna un mensaje con la cantidad de archivos editados y el body null
 * @author Maria Vera <'mafervera.1203@gmail.com'>
 */

device.updateDevice = async(req, res = response) => {
    try {
        const body = req.body;
        const id = req.params.id;
        let argumentsUpd = {};

        //------ validamos los parametros
        if (!id || !regex.test(id)) {
            return res.status(400).json({
                ok: false,

                message: "El id debe ser un entero",
                body: null,
            });
        }

        if (body["device"]) argumentsUpd["name-device"] = body["device"];
        if (body["lat"]) argumentsUpd["lat-device"] = body["lat"];
        if (body["lon"]) argumentsUpd["lon-device"] = body["lon"];
        if (body["description"])
            argumentsUpd["description-luminary"] = body["description"];
        if (body["voltage"]) argumentsUpd["normal-shunt-voltage"] = body["voltage"];
        if (body["current"]) argumentsUpd["normal-current"] = body["current"];
        if (body["power"]) argumentsUpd["normal-power"] = body["power"];
        if (body["working"]) argumentsUpd["working"] = body["working"];
        if (body["address"]) argumentsUpd["id-address"] = body["address"];

        await DeviceModel.update(
                argumentsUpd,

                {
                    where: {
                        "id-device": id,
                    },
                }
            )
            .then((resp) => {
                return res.status(200).json({
                    ok: true,

                    message: `se actualizaron ${resp} registros`,
                    body: null,
                });
            })
            .catch((e) => {
                throw new Error(e);
            });
    } catch (err) {
        throw new Error(err);
    }
};

/*
 
  ######   ######## ########     ######   #######  ##     ## ##     ## ##    ##    ###     ######  
 ##    ##  ##          ##       ##    ## ##     ## ###   ### ##     ## ###   ##   ## ##   ##    ## 
 ##        ##          ##       ##       ##     ## #### #### ##     ## ####  ##  ##   ##  ##       
 ##   #### ######      ##       ##       ##     ## ## ### ## ##     ## ## ## ## ##     ##  ######  
 ##    ##  ##          ##       ##       ##     ## ##     ## ##     ## ##  #### #########       ## 
 ##    ##  ##          ##       ##    ## ##     ## ##     ## ##     ## ##   ### ##     ## ##    ## 
  ######   ########    ##        ######   #######  ##     ##  #######  ##    ## ##     ##  ######  
 
*/

device.getComunas = async(req, res = response) => {
    try {
        let comunas = [];
        const resp = await CommuneModel.findAll({
            attributes: ["id-commune", "name-commune"],
        }, { raw: true });
        if (resp === null) {
            comunas = null;
        } else {
            comunas = resp.map((value) => value.dataValues);
        }

        return res.status(200).json({
            ok: true,
            message: "Ok",
            body: comunas,
        });
    } catch (e) {
        return res.status(500).json({
            ok: false,
            message: `${e}`,
            body: null,
        });
    }
};

/*
**********************************
Fault of device  UPDATE FIX
**********************************
*/

device.updateFault = async(req, res) => {
    if (req.id_rol === 3)
        return res
            .status(401)
            .json({ ok: false, body: "Don't have authentication" });
    try {
        const id = req.params.idFailure;
        if (!id) {
            return res.status(200).json({
                ok: false,
                msg: "El id de failure  es requerido",
            });
        }
        let email = await querys.getIdEmployee(req.email_user);
        let status = await querys.statusFailure(id);
        if (!status.length) {
            return res.status(200).json({
                ok: false,
                msg: "El id de failure no existe",
            });
        }
        if (status[0]["status-failure"] === 1) {
            await querys.updateFailure(id, email[0]["id-employee"]);
            await querys.workUpdateOn(status[0]["id-device"]);
            res.json({
                ok: true,
                message: "status update",
            });
        }
        if (status[0]["status-failure"] === 2) {
            res.json({
                ok: true,
                message: "the failure has been fixed",
            });
        }
    } catch (error) {
        throw error;
    }
};

module.exports = device;