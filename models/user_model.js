const Sequelize = require("sequelize");

const conn = require("../database/conectar_db");

const RolModel = require("../models/rol_model");

const UserModel = conn.define(
    "user", {
        "email-user": {
            type: Sequelize.STRING(32),
            allowNull: false,
            primaryKey: true,
            unique: true,
            validate: {
                isEmail: {
                    msg: "El email tiene que ser un correo valido",
                },
            },
        },

        "password-user": {
            type: Sequelize.STRING(128),
            is: /^[0-9a-f]{64}$/i,

            allowNull: false,
        },
        "name-user": {
            type: Sequelize.STRING,
            allowNull: false,
            validate: {
                len: {
                    args: [2, 255],
                    msg: "El nombre tiene que ser minimamente de dos caracteres",
                },
            },
        },
    }, {
        //------- NO AGREGA EL CREATEAT Y UPDATEAT --------------------//
        timestamps: false,
        underscored: false,
        //------- EVITA QUE CAMBIE EL NOMBRE DE LA TABLA --------------//
        freezeTableName: true,
        //------- NOMBRE DE LA TABLA EN LA BASE DE DATOS --------------//
        tableName: "user",
    }
);

// Relación 1:N entre Rol y User
UserModel.belongsTo(RolModel, { foreignKey: "id-rol", constraints: false });
RolModel.hasMany(UserModel, { foreignKey: "id-rol", constraints: false });

module.exports = UserModel;